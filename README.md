In the AWS console we will go to ElastiCache and we will click on the button “Get Started Now”.

After starting the process of creating a new cluster of ElastiCache we will indicate that we want to use the Redis engine without the Cluster Mode, we will indicate a name, change the node type to a t2.micro (free tier) and set the Number of replicas to “None”.

![](01.png)


Next we will set the advanced settings, where we will do a couple of things: the first one is to define a group of subnets through which the cluster can be deployed (always a minimum of two subnets is recommended in a group, in this case I added three private subnets of default VPC).

![](02.png)

The following step is to indicate a Security Group, for this demo I have created a specific one.

![](03.png)

![](04.png)

# create ec2 instance in same VPC and security group

curl -sL https://raw.githubusercontent.com/prabhatpankaj/ubuntustarter/master/initial.sh | sh

sudo apt-get install build-essential tcl

sudo apt-get install redis-server

sudo apt-get install python-pip

sudo pip install --upgrade setuptools pip

pip install --upgrade redis

# enter into python shell

Python 2.7.12 (default, Dec  4 2017, 14:50:18) 
[GCC 5.4.0 20160609] on linux2
Type "help", "copyright", "credits" or "license" for more information.

>>> import redis

>>> r = redis.Redis(host='redislab.slzbxj.0001.apse1.cache.amazonaws.com', port=6379, db=0)

>>> r.set('foo','bar')

True

>>> r.get('foo')

'bar'

# using redis-cli

redis-cli -h redislab.slzbxj.0001.apse1.cache.amazonaws.com

redislab.slzbxj.0001.apse1.cache.amazonaws.com:6379> MSET firstname Jack lastname Stuntman age 35
OK
redislab.slzbxj.0001.apse1.cache.amazonaws.com:6379> KEYS *name*
1) "lastname"
2) "firstname"
redislab.slzbxj.0001.apse1.cache.amazonaws.com:6379> KEYS *
1) "age"
2) "lastname"
3) "firstname"
4) "foo"
redislab.slzbxj.0001.apse1.cache.amazonaws.com:6379>








